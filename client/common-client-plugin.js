function register({ registerHook, peertubeHelpers }) {
  registerHook({
    target: "action:router.navigation-end",
    handler: async ({ path }) => {
      const c = await peertubeHelpers.getSettings(
        "custom_links_header_markdown"
      );
      const panel = document.createElement("div");
      panel.setAttribute("class", "custom-links-header");
      if (c["custom_links_header_markdown"]) {
        const html = await peertubeHelpers.markdownRenderer.enhancedMarkdownToHTML(
          c["custom_links_header_markdown"]
        );
        panel.innerHTML = html;
      }
      setInterval(async function () {
        const customLinks = document.querySelector(".custom-links-header");
        const container = document.querySelector(".header");
        if (customLinks === null && c["custom_links_header_markdown"]) {
          container.appendChild(panel);
        }
        //add focus event
        document.querySelector('my-global-icon.icon.icon-search').onclick=function(){
          document.getElementById('search-video').focus()
          }
      }, 100);
    },
  });
  registerHook({
    target: "action:router.navigation-end",
    handler: async ({ path }) => {
      setInterval(async function () {
        if (
          !document.querySelector(".custome-login-button") &&
          document.querySelector(".menu-wrapper")
        ) {
          const arr = document.querySelectorAll(".footer-links div a");
          const el = arr[arr.length - 2];
          el.innerText = "Login";
          el.href = "/login";
          el.target = "_self";
          el.className = "custome-login-button";
        }
      }, 100);
    },
  });
  registerHook({
    target: "action:router.navigation-end",
    handler: async({ path }) => {
        if (!document.querySelector("#realitytube-logo")) {
          
            const logoEl = document.querySelector("#custom-css .icon.icon-logo");
            logoEl.id = "realitytube-logo";
            const favicon = document.querySelector('link[rel="icon"]')
            const pathStatic= await peertubeHelpers.getBaseStaticRoute()

            favicon.href = pathStatic+'/images/logo.svg'
          
        }
    },
});
  registerHook({
    target: "action:router.navigation-end",
    handler: async ({ path }) => {
    try {
        const serverConfig = JSON.parse(localStorage.getItem("server-config"));

        serverConfig.theme.registered = serverConfig.theme.registered.filter(
          (item) =>
            item.name === "realitytube-dark" || item.name === "realitytube-light"
        );

        localStorage.setItem("server-config",JSON.stringify(serverConfig))
    } catch (error) {

    }

    },
  });
  registerHook({
    target: "action:router.navigation-end",
    handler: async ({ path }) => {
      document.body.style.opacity = 1;
    },
  });
}

export { register };
