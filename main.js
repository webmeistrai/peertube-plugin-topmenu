async function register ({
  registerHook,
  registerSetting,
  settingsManager,
  storageManager,
  videoCategoryManager,
  videoLicenceManager,
  videoLanguageManager,
  peertubeHelpers,
}) {
  registerSetting({
    name: 'custom_links_header_markdown',
    label: 'Custom Links Header Markdown',
    type: 'markdown-enhanced',
    private: false,
    default: ''
  })
}

async function unregister () {
  return
}

module.exports = {
  register,
  unregister
}
